// Wow & animate 
new WOW().init();

// Scroll pages 
document.querySelectorAll('a[href^="#"').forEach(link => {
    link.addEventListener('click', function(e) {
        e.preventDefault();

        let href = this.getAttribute('href').substring(1);
        const scrollTarget = document.getElementById(href);
        const topOffset = 30; 
        const elementPosition = scrollTarget.getBoundingClientRect().top;
        const offsetPosition = elementPosition - topOffset;

        window.scrollBy({
            top: offsetPosition,
            behavior: 'smooth'
        });
    });
});

// Overlay screen //
const overlay = document.querySelector('.overlay');

// Navigation mobile menu //
const navigation = document.querySelector('.header__navigation');
const navMenu = document.querySelector('.product__menu');
const product = document.querySelector('.header__navigation');
const item = document.querySelectorAll('.product__wrapper');

navMenu.addEventListener('click', () => {
    product.classList.toggle('header__navigation--active')
    overlay.classList.toggle('overlay--active');
});

// Navigation menu items // 
btns = item;
  btns.forEach(function (el) {
    el.addEventListener('click', () => {
        product.classList.remove('header__navigation--active')
        overlay.classList.remove('overlay--active');
    });
  })

// Modal window - shopping cart
const basketModal = document.querySelector('.basket__modal');
const basketOpen = document.querySelector('.header__basket');
const basketClose = document.querySelector('.basket__close');

basketOpen.addEventListener('click', ()=> {
  basketModal.classList.toggle('basket__modal--active');
  overlay.classList.toggle('overlay--active');
  navMenu.style.zIndex = "2";
});

basketClose.addEventListener('click', ()=> {
  basketModal.classList.remove('basket__modal--active');
  overlay.classList.remove('overlay--active');
  navMenu.style.zIndex = "5";
});

// Modal window - Delivery
const deliveryModal = document.querySelector('.delivery__modal');
const deliveryOpen = document.querySelector('.delivery__open');
const deliveryPrevious = document.querySelector('.delivery__previous');

deliveryOpen.addEventListener('click', () => {
    basketModal.classList.remove('basket__modal--active');
    deliveryModal.classList.toggle('delivery__modal--active');
});

deliveryPrevious.addEventListener('click', () => {
    deliveryModal.classList.remove('delivery__modal--active');
    basketModal.classList.toggle('basket__modal--active');
});
 
// Modal window - Delivery - Working with the form //
// Select form
const selectElements = document.querySelectorAll('.js-selectCustom');
const onCloseSelect = (control, target) => {
    const didClickedOutside = !control.contains(target);
    if (didClickedOutside) {
        control.classList.remove('isActive');
    }
};

const onOpenSelect = (control) => control.classList.toggle("isActive");
const onChangeSelect = (option, selectControl) => {
    const triggelControl = selectControl.querySelector('.selectCustom-trigger');
    triggelControl.textContent = option.textContent;
    onCloseSelect(selectControl);
}

document.addEventListener("click", (evt) => {
    selectElements.forEach((el) => onCloseSelect(el, evt.target));
});

selectElements.forEach((el) => {
    const selectControl = el;
    const optionsList = selectControl.querySelectorAll('.selectCustom-option');
    const triggelControl = selectControl.querySelector('.selectCustom-trigger');

    triggelControl.addEventListener("click", () => onOpenSelect(selectControl));
    optionsList.forEach((option) => {
        option.addEventListener("click", () => onChangeSelect(option, selectControl));
    })
});

document.addEventListener("DOMContentLoaded", function(event) {
    const cartButtons = document.querySelectorAll('.add-to-basket__button');
    cartButtons.forEach(button => {
    button.addEventListener('click',cartClick);
    });
    
    function cartClick(){
        let button =this;
        button.classList.add('clicked');
        setTimeout(function(){
          button.classList.remove('clicked');
        },2500);
    } 
});

// Modal window - Order completed
const orderModal = document.querySelector('.order__modal');
const orderButton = document.querySelector('.delivery__order');
const orderClose = document.querySelector('.placed__order');

orderButton.addEventListener('click', ()=> {
    deliveryModal.classList.remove('delivery__modal--active');
    orderModal.classList.toggle('order__modal--active');
});

orderClose.addEventListener('click', ()=> {
    orderModal.classList.remove('order__modal--active');
    overlay.classList.remove('overlay--active');
});

// Main slider
var swiper = new Swiper(".mySwiper", {
    autoplay: {
        delay: 2500,
    },

    navigation: {
        nextEl: ".swiper-button-next",
        prevEl: ".swiper-button-prev",
    },
});

// Scroll to top
var scrollToTopBtn = document.querySelector(".scroll-to-top__button ");
var rootElement = document.documentElement;

function handleScroll() {
  
  var scrollTotal = rootElement.scrollHeight - rootElement.clientHeight;
  if (rootElement.scrollTop / scrollTotal > 0.1) {
    
    scrollToTopBtn.classList.add("scroll-to-top__button--show");
  } else {
   
    scrollToTopBtn.classList.remove("scroll-to-top__button--show");
  }
}

function scrollToTop() {
  rootElement.scrollTo({
    top: 0,
    behavior: "smooth"
  });
}

scrollToTopBtn.addEventListener("click", scrollToTop);
document.addEventListener("scroll", handleScroll);

// Working with the basket: adding, deleting, editing // -- Important script
const productsBtn = document.querySelectorAll('.add-to-basket__button');
const cartProductsList = document.querySelector('.basket__product-list');
const cart = document.querySelector('.header__basket');
const cartQuantity = document.querySelector('.counter');
const fullPrice = document.querySelector('.basket__amount');
let price = 0;

function num_word(value, words) {  
	value = Math.abs(value) % 100; 
	var num = value % 10;
	if(value > 10 && value < 20) return words[2]; 
	if(num > 1 && num < 5) return words[1];
	if(num == 1) return words[0]; 
	return words[2];
}

const cartModule = {
    'count': 0,
    
    'total': 0,

    'list': {},

    update: function() {
        const localStorage = this.getLocalStorage();

        if (localStorage) {
            this.count = localStorage.count;
            this.total = localStorage.total;
            this.list = localStorage.list;
        }

        this.changeMiniCartCount();
        this.updateCartList();
    },

    getLocalStorage: function () {
        return window.localStorage.getItem('cart') 
            ? JSON.parse(window.localStorage.getItem('cart'))
            : false
    },

    setLocalStorge: function () {
        window.localStorage.setItem('cart', JSON.stringify({
            count: this.count,
            total: this.total,
            list: this.list
        }));
    },

    updateCount: function () {
        this.count = 0;
        for (let id in this.list) {
            this.count += this.list[id]['quantity']
        }
    },

    updateTotal: function () {
        this.total = 0;
        for (let id in this.list) {
            this.total += this.list[id]['total']
        }
    },

    add: function (id, price, title, image, quantity) {
        let product_quantity = quantity;
    
        if (id in this.list) {
            product_quantity = this.list[id]['quantity'] + quantity;
        }
        
        this.list[id] = {
            'id': id, 
            'price': price,
            'title': title,
            'image': image,
            'quantity': product_quantity,
            'total': price * product_quantity
        }

        this.updateCount();
        this.updateTotal();
        this.setLocalStorge();
        this.update();
    },

    remove: function (id) {
        delete this.list[id];
        
        this.updateCount();
        this.updateTotal();
        this.setLocalStorge();
        this.update();
    },

    increment: function (id) {
        this.list[id].quantity += 1;
        this.list[id].total = this.list[id].quantity * this.list[id].price;
        this.updateCount();
        this.updateTotal();
        this.setLocalStorge();
        this.update();
    },
    
    decrement: function (id) {
        this.list[id].quantity -= 1;
        if (this.list[id].quantity === 0) {
            this.remove(id)
        } else {
            this.list[id].total = this.list[id].quantity * this.list[id].price;
        }
        this.updateCount();
        this.updateTotal();
        this.setLocalStorge();
        this.update();
    },

    clear: function () {
        this.count = 0;
        this.total = 0;
        this.list = {};

        this.changeMiniCartCount();
        this.updateCartList();
        this.setLocalStorge();
    },

    changeMiniCartCount() {
        document.querySelector('.counter').textContent = this.count;
        // document.querySelector('.mobile-basket__quantity').textContent = this.count;
    },

    updateCartList() {
        let cartHtml = '';
        
        for (let productId in this.list) {
            let product = this.list[productId];
            cartHtml += this.templateMiniCartProduct(product);
        }
        
        document.querySelector('.basket__amount').textContent = this.total;
        document.querySelector('.delivery__amount').textContent = this.total;
        document.querySelector('.basket__product-counter').textContent = `${this.count} ${num_word(this.count, ['товар', 'товара', 'товаров'])}`;
        document.querySelector('.basket__product-list').innerHTML = cartHtml;

        setRemoveProductFromCartEvents();
    },

    templateMiniCartProduct: (product) => {
        return `
            <div class="basket-card js-mini-cart-item" data-id="${product.id}">
                <div class="basket-card__wrapper">
                    <img class="basket-card__image" src="${product.image}" alt="card-image">
                    <div class="basket-card__description">
                        <h4 class="basket-card__title">${product.title}</h4>
                        <p class="basket-card__vendor-code">${product.id}</p>
                    </div>
                </div>
                <div class="basket-card__wrapper">
                    <div class="basket-card__control">
                        <button class="basket-card__plus-minus js-cart-minus">
                            <img class="basket-card__plus-minus-image" data-id="minus" src="img/icon/minus.svg" alt="minus">
                        </button>
                        <span class="basket-card__quantity">${product.quantity}</span>
                        <button class="basket-card__plus-minus js-cart-plus" data-id="plus">
                            <img class="basket-card__plus-minus-image" src="img/icon/plus.svg" alt="plus">
                        </button>
                    </div>
                    <p class="basket-card__price">${product.price}<span>&nbsp;₽</span></p>
                </div>
            </div>
        `;
    }
}

cartModule.update();

document.querySelectorAll('.js-add-to-cart').forEach(el => {
    el.addEventListener('click', (e) => {
        let self = e.currentTarget;
        cartModule.add(
            self.dataset.id,
            self.dataset.price,
            self.dataset.title,
            self.dataset.image,
            1
        )
    });
});

function setRemoveProductFromCartEvents () {
    document.querySelectorAll('.js-remove-from-cart').forEach(el => {
        el.addEventListener('click', (e) => {
            let self = e.currentTarget;
            const card = self.closest('.js-mini-cart-item')
            cartModule.remove(card.dataset.id);
        });
    });
    document.querySelectorAll('.js-cart-minus').forEach(el => {
        el.addEventListener('click', (e) => {
            let self = e.currentTarget;
            const card = self.closest('.js-mini-cart-item')
            cartModule.decrement(card.dataset.id);
        });
    });
    document.querySelectorAll('.js-cart-plus').forEach(el => {
        el.addEventListener('click', (e) => {
            let self = e.currentTarget;
            const card = self.closest('.js-mini-cart-item')
            cartModule.increment(card.dataset.id);
        });
    });
    document.querySelectorAll('.js-return-to-cart').forEach(el => {
        el.addEventListener('click', (e) => {
            let self = e.currentTarget;
            const card = self.closest('.js-mini-cart-item')
            console.log(card.dataset.id);
        });
    });
}

document.querySelector('.js-clear-cart').addEventListener('click', (e) => {
    cartModule.clear()
})